package com.company;

import java.util.ArrayList;

public class Baralho {

    private String[] nomes = {"Ás", "2", "3", "4", "5", "6", "7", "8","9", "10", "Dama", "Valete", "Rei"};
    private String[] naipes = {"Ouros", "Espadas", "Copas", "Paus"};

    private ArrayList<Carta> cartas = new ArrayList<>();

    public Baralho() {
        embaralhar();
    }

    public Carta retornarCarta() {
        return this.cartas.remove(Randomizador.getInt(this.cartas.size()));
    }

    private void embaralhar() {
        for (int i = 0; i < nomes.length; i++) {
            for (int j = 0; j < naipes.length; j++) {
                cartas.add(new Carta(nomes[i], naipes[j], i > 9 ? 10 : i + 1));
            }
        }
    }
}
