package com.company;

public class Carta {

    private String nome;
    private String naipe;
    private int pontos;

    public Carta(String nome, String naipe, int pontos) {
        this.nome = nome;
        this.naipe = naipe;
        this.pontos = pontos;
    }

    public int getPontos() {
        return pontos;
    }

    public String getNome() {
        return nome;
    }

    public String getNaipe() {
        return naipe;
    }
}
