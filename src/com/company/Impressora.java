package com.company;

public class Impressora {

    public static void imprimirCartaSorteada(Carta carta) {
        System.out.println("Carta sorteada: " + carta.getNome() + " de " + carta.getNaipe() + " - " + carta.getPontos());
    }

    public static void perder() {
        System.out.println("Você perdeu!");
    }

    public static void ganhar() {
        System.out.println("Você ganhou!");
    }

    public static void sair() {
        System.out.println("Jogo encerrado!");
    }

    public static void desistir() {
        System.out.println("Você desistiu!");
    }

    public static void imprimirHistorico(int historico) {
        System.out.println("------------- HISTÓRICO -------------");
        System.out.println(historico + " pontos");
        System.out.println();
    }

    public static void imprimirPontuacao(int pontuacao) {
        System.out.println("Pontuação total: " + pontuacao);
    }
}
