package com.company;

public class Jogo21 {

    private static final int LIMITE = 21;

    private int pontuacaoJogador;
    private int historico;

    private Baralho baralho = new Baralho();

    public void iniciarJogo() {
        Entrada entrada = new Entrada();
        String resposta = "";

        do {
            resposta = entrada.recuperarPerguntaNovaCarta();
            realizarJogada(resposta);
        } while (!resposta.equals(Constantes.RESPOSTA_SAIR));
    }

    private void realizarJogada(String resposta) {
        if (resposta.equals(Constantes.RESPOSTA_SIM)) {
            Carta cartaComprada = baralho.retornarCarta();

            this.pontuacaoJogador += cartaComprada.getPontos();

            if(this.pontuacaoJogador > LIMITE) {
                jogadaMaiorLimite(cartaComprada);
            } else if (this.pontuacaoJogador == LIMITE) {
                jogadaIgualLimite(cartaComprada);
            } else {
                jogadaMenorLimite(cartaComprada);
            }

        } else if (resposta.equals(Constantes.RESPOSTA_NAO)) {
            jogadaNao();
        } else if (resposta.equals(Constantes.RESPOSTA_SAIR)) {
            jogadaSair();
        }
    }

    private void jogadaSair() {
        Impressora.sair();
        System.exit(1);
    }

    private void jogadaNao() {
        Impressora.desistir();
        Impressora.imprimirPontuacao(pontuacaoJogador);
        atualizarHistorico();
        Impressora.imprimirHistorico(this.historico);
        resetar();
    }

    private void jogadaMenorLimite(Carta cartaComprada) {
        Impressora.imprimirCartaSorteada(cartaComprada);
        Impressora.imprimirPontuacao(pontuacaoJogador);
        atualizarHistorico();
    }

    private void jogadaIgualLimite(Carta cartaComprada) {
        Impressora.ganhar();
        Impressora.imprimirCartaSorteada(cartaComprada);
        Impressora.imprimirPontuacao(pontuacaoJogador);
        atualizarHistorico();
        Impressora.imprimirHistorico(this.historico);
        resetar();
    }

    private void jogadaMaiorLimite(Carta cartaComprada) {
        Impressora.imprimirCartaSorteada(cartaComprada);
        Impressora.perder();
        Impressora.imprimirPontuacao(pontuacaoJogador);
        Impressora.imprimirHistorico(this.historico);
        resetar();
    }

    private void atualizarHistorico() {
        if (this.historico < this.pontuacaoJogador) {
            this.historico = this.pontuacaoJogador;
        }
    }

    private void resetar() {
        this.pontuacaoJogador = 0;
        this.baralho = new Baralho();
    }
}
